//
//  ViewController.swift
//  IOS_TCP_IP_IO_Client
//
//  Created by ALAN SOMMERVILLE on 19/7/17.
//  Copyright © 2017 ALAN SOMMERVILLE. All rights reserved.
//

import UIKit
import AudioToolbox   // Used for General Audio

// Variable used for TCP Connectivity (variable for DATA; Streams and Buffers
var iData    = "Test Text\n"
var sendData = iData.data(using: String.Encoding.utf8)
var uData    = sendData?.withUnsafeBytes({ (buffer:UnsafePointer<UInt8>?) -> UnsafePointer<UInt8> in return buffer!.advanced(by: 0)})
var inputStream:InputStream!
var outputStream:OutputStream!
var buffer = [UInt8](repeating: 0, count: 64)

// Other misc variables
var varCONNECT_STATUS = false    // Tracks the connection status
var varRECEIVED = ""             // For holding received text


class ViewController: UIViewController, StreamDelegate
{
    
    // Outlets for the buttons
    @IBOutlet weak var textFIELD_IP: UITextField!
    @IBOutlet weak var textFIELD_PORT: UITextField!
    @IBOutlet weak var buttonFIELD_CONNECT: UIButton!
    @IBOutlet weak var textFIELD_STATUS: UILabel!
    @IBOutlet weak var buttonFIELD_SEND: UIButton!
    @IBOutlet weak var textFIELD_textsend: UITextField!
    @IBOutlet weak var textFIELD_RECEIVED: UILabel!
  
    
    // Actions which occur when the button is pressed
    @IBAction func Connect_Pressed(_ sender: Any)
    {
        AudioServicesPlaySystemSound(1362)
        useCFStream()
    }
    
    
    @IBAction func Send_Pressed(_ sender: Any)
    {
        // Send this data in the TEXT BOX to the stream
        iData    = textFIELD_textsend.text!
        sendData = iData.data(using: String.Encoding.utf8)
        uData    = sendData?.withUnsafeBytes({ (buffer:UnsafePointer<UInt8>?) -> UnsafePointer<UInt8> in return buffer!.advanced(by: 0)})
        
        // Send the data to the OUTPUT STREAM
        outputStream.write(uData!, maxLength: sendData!.count)
        NSLog("Constructed string = \(iData)")
    }
    
    
    // Function to setup the INPUT/OUTPUT streams
    func useCFStream()
    {
        // Setup the Variable for the connection
        let host:CFString = String(textFIELD_IP.text!)! as CFString
        let port:UInt32 = UInt32(textFIELD_PORT.text!)!
        
        var readStream  : Unmanaged<CFReadStream>?
        var writeStream : Unmanaged<CFWriteStream>?
        
        // Open the Stream
        CFStreamCreatePairWithSocketToHost(nil, host as CFString, port, &readStream, &writeStream)
        
        // Retrain the variable which comes back so can be reference later
        inputStream             = readStream!.takeRetainedValue()
        outputStream            = writeStream!.takeRetainedValue()
        
        inputStream.delegate    = self
        outputStream.delegate   = self
        
        // Schedule IN RUNLOOP
        inputStream.schedule(in: RunLoop.main, forMode:.defaultRunLoopMode)
        outputStream.schedule(in: RunLoop.main, forMode:.defaultRunLoopMode)

        NSLog ("LOG: Connecting to IP: \(host) PORT: \(port)")
        
        // Open Streams - INPUT and OUTPUT
        inputStream.open()
        outputStream.open()
        
        NSLog ("Function complete : useCFStream")
    }
    
    
    // Function which handles events that happen on the TCP Stream between iPhone and Raspberry Pi (or other TCP/IP client)
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        
        if aStream === inputStream
        {
            switch eventCode
            {
                
            case Stream.Event.openCompleted:
                NSLog("LOG: input: OpenCompleted")
                textFIELD_STATUS.text = "CONNECTED"
                textFIELD_STATUS.backgroundColor = UIColor.green
                varCONNECT_STATUS = true
                break
                
            case Stream.Event.errorOccurred:
                NSLog("LOG: input: ErrorOccurred : \(String(describing: aStream.streamError?.localizedDescription))")
                textFIELD_STATUS.text = "CONNECT LOST"
                textFIELD_STATUS.backgroundColor = UIColor.red
                varCONNECT_STATUS = false
                break
                
            case Stream.Event.endEncountered:
                NSLog("LOG: EndEncountered")
                textFIELD_STATUS.text = "CONNECT LOST"
                textFIELD_STATUS.backgroundColor = UIColor.red
                varCONNECT_STATUS = false
                break
                
            case Stream.Event.hasBytesAvailable:
                NSLog("LOG: input: HasBytesAvailable")
                while inputStream.hasBytesAvailable
                {
                    inputStream.read(&buffer, maxLength: buffer.count)
                    varRECEIVED = String(bytes: buffer, encoding: String.Encoding.utf8)!
                }
                textFIELD_RECEIVED.text = varRECEIVED
                NSLog("LOG: Text Received = \(varRECEIVED)")
                break
                
            case Stream.Event.hasSpaceAvailable:
                NSLog("LOG: HasSpaceAvailable")
                break
                
            default:
                NSLog("LOG: default")
                break
            }
        }
            
        else if aStream === outputStream
        {
            NSLog("LOG: Stream - Output event occured")
        }
            
        else
        {
            NSLog("LOG: Streeam - Unknown event occured")
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

